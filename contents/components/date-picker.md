---
name: Date picker
related:
  - forms
  - dropdowns
---

Date picker allows users to choose and input a date through by manually typing the date into the input field or by using a calendar-like dropdown.

## Usage

The date picker comes in two parts: the input field and the date picker dropdown. The input field must use a placeholder text to indicate the expected format of time to be entered (e.g. "YYYY-MM-DD") and a calendar icon on the right edge to indicate that clicking on it will open a datepicker dropdown.

The user should be able to input the date by either typing it in or choosing a day from the datepicker dropdown. The user must never be forced to use only one of the two input methods.

Todo: Add live component block with code example (datepicker: input field + dropdown)

## Design specification

Color, spacing, dimension, and layout specific information pertaining to this component can be viewed using the following link:

[Sketch Measure Preview for the Date picker](https://gitlab-org.gitlab.io/gitlab-design/hosted/design-gitlab-specs/datepicker-spec-previews/)
